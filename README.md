# Aliyun::SLB::Utility

阿里云负载均衡监听管理工具.支持配置的复制,备份和恢复

## 安装

在 Gemfile 中增加如下代码:

```ruby
gem 'Aliyun-SLB-Utility'
```

执行:

    $ bundle

或者自行安装:

    $ gem install Aliyun-SLB-Utility

## 使用

运行`SLB_Utility`命令,详细可以输入`-h`查看.

## 贡献

有问题欢迎来提交issue http://git.oschina.net/doraemon/Aliyun-SLB-Utility.

