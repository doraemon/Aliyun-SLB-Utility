# Copyright 2015 ZhangZhaoyuan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'aliyunsdk/core/utils/api_util'

module Aliyun
  module SLB
    module Utility
      class Utils
        # 自动通过当前负载均衡实例信息,给新创建的请求赋值
        # @param create_request [Request] 创建请求的对象
        # @param info [Response] 当前实例信息
        # @return [Request] 创建请求的对象
        def self.duplicate_params(create_request, info)
          # 循环当前实例信息
          info.each_pair do |k, v|
            # 判断是否定义有当前字段,如果有,则设置相应的值
            if create_request.class.public_method_defined? "#{Aliyunsdk::Core::ApiUtil.underscore(k)}="
              create_request.send :"#{Aliyunsdk::Core::ApiUtil.underscore(k)}=", v
            end
          end
          create_request
        end
      end
    end
  end
end