# Copyright 2015 ZhangZhaoyuan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'Aliyun/SLB/Utility/utils'

module Aliyun
  module SLB
    module Utility

      # 复制监听配置,支持批量复制
      class DuplicateListenerConfig

        def initialize(**options)
          @options = options
        end

        def do_action
          # 根据监听类型创建不同的请求
          case @options[:type]
            when :http
              require 'aliyunsdk/slb/request/v20140515/describe_load_balancer_http_listener_attributes'
              require 'aliyunsdk/slb/request/v20140515/create_load_balancer_http_listener'
              request = Aliyunsdk::Slb::DescribeLoadBalancerHTTPListenerAttributes.new 'JSON'
              create_request = Aliyunsdk::Slb::CreateLoadBalancerHTTPListener.new 'JSON'
            when :https
              require 'aliyunsdk/slb/request/v20140515/describe_load_balancer_https_listener_attribute'
              require 'aliyunsdk/slb/request/v20140515/create_load_balancer_https_listener'
              request = Aliyunsdk::Slb::DescribeLoadBalancerHTTPSListenerAttribute.new 'JSON'
              create_request = Aliyunsdk::Slb::CreateLoadBalancerHTTPSListener.new 'JSON'
            when :tcp
              require 'aliyunsdk/slb/request/v20140515/describe_load_balancer_tcp_listener_attribute'
              require 'aliyunsdk/slb/request/v20140515/create_load_balancer_tcp_listener'
              request = Aliyunsdk::Slb::DescribeLoadBalancerTCPListenerAttribute.new 'JSON'
              create_request = Aliyunsdk::Slb::CreateLoadBalancerTCPListener.new 'JSON'
            when :udp
              require 'aliyunsdk/slb/request/v20140515/describe_load_balancer_udp_listener_attribute'
              require 'aliyunsdk/slb/request/v20140515/create_load_balance_udp_listener'
              request = Aliyunsdk::Slb::DescribeLoadBalancerUDPListenerAttribute.new 'JSON'
              create_request = Aliyunsdk::Slb::CreateLoadBalanceUDPListener.new 'JSON'
            else
              puts 'Listener type is required!'
              exit
          end

          # 通过传入的第一个负载均衡实例ID获取信息
          request ||= nil
          if request and @options[:bids] and @options[:port]
            request.load_balancer_id = @options[:bids].first
            request.listener_port = @options[:port]
            res = @options[:client].do_action(request)
          end

          res ||= nil
          create_request ||= nil
          # 判断输入的负载均衡实例ID下是否有监听存在
          if res.nil? or res['Code'] or create_request.nil?
            puts "Get load balancer info failed! the response is \n#{res}"
            exit
          else
            # 循环创建监听
            @options[:bids][1..-1].each do |balancer_id|
              # 自动复制参数到请求对象中
              create_request = Utils.duplicate_params create_request, res
              create_request.load_balancer_id = balancer_id
              create_res = @options[:client].do_action create_request
              puts "Load Balancer Id(#{balancer_id}) create response:#{create_res}"
            end
          end

          puts "Duplicate Config Successful!"
        end
      end
    end
  end
end