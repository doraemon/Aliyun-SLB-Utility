# Copyright 2015 ZhangZhaoyuan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require "Aliyun/SLB/Utility/version"
require 'optparse'
require 'Aliyun/SLB/Utility/duplicate_listener_config'
require 'Aliyun/SLB/Utility/backup_listener_config'
require 'Aliyun/SLB/Utility/recover_listener_config'
require 'aliyunsdk/core/client'

module Aliyun
  module SLB
    module Utility
      include Aliyunsdk::Core::Client

      def self.parse_command_line
        action = nil
        options = {}
        option_parser = OptionParser.new do |opts|
          opts.banner = 'Usage: SLB_Utility Action Argument'

          opts.separator ''
          opts.separator 'Action options:'

          opts.on('-a ACTION', '--action Action', [:duplicate, :backup, :recover],
                  'Select action (duplicate, backup, recover)') do |action|
            options[:action] = action
          end

          opts.separator ''
          opts.separator 'Argument options:'

          opts.on('-t TYPE', '--type Type', [:http, :https, :tcp, :udp],
                  'Select listener type (http, https, tcp, udp)') do |t|
            options[:type] = t
          end

          opts.on('-bids id1,id2,id3', '--balancer_ids id1,id2,id3', Array, 'LoadBalancerIds.The first id is the current balancer.') do |ids|
            options[:bids] = ids
          end

          opts.on('-p PORT', '--port Port', 'Listen Port') do |port|
            options[:port] = port
          end

          opts.separator ''
          opts.separator 'Authentication options:'

          opts.on('-k KEY', '--key Key', 'Your aliyun api access key id') do |access_key_id|
            options[:access_key_id] = access_key_id
          end

          opts.on('-s SECRET', '--secret Secret', 'Your aliyun api access key secret') do |secret|
            options[:access_key_secret] = secret
          end
        end

        option_parser.parse!

        if options[:access_key_id] and options[:access_key_secret]
          AcsClient.access_key_id = options[:access_key_id]
          AcsClient.access_key_secret = options[:access_key_secret]
          options[:client] = AcsClient.new
        end

        case options[:action]
          when :duplicate
            action = DuplicateListenerConfig.new options
          when :backup
            action = BackupListenerConfig.new options
          when :recover
            action = RecoverListenerConfig.new options
          else
            puts 'Action is required!'
            puts option_parser
            exit
        end

        action.do_action unless action.nil?
      end
    end
  end
end
