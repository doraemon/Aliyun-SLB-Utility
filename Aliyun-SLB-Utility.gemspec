# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'Aliyun/SLB/Utility/version'

Gem::Specification.new do |spec|
  spec.name = "Aliyun-SLB-Utility"
  spec.version = Aliyun::SLB::Utility::VERSION
  spec.authors = ["ZhangZhaoyuan"]
  spec.email = ["doraemon.zh@gmail.com"]

  spec.summary = %q{Manager for listener of aliyun-slb.}
  spec.description = %q{Copy,backup and recover the listener config to file.}
  spec.homepage = "http://git.oschina.net/doraemon/aliyun-openapi-ruby-sdk"

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir = "bin"
  spec.executables = spec.files.grep(%r{^SLB/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest"

  spec.add_runtime_dependency "aliyunsdk-slb", ">=0.1.0"
end
